## Como importar

1. Isolate only the questions array in json file
2. Replace "$" by "@value"
3. Replace "id" by "_id"
3. mongoimport -v -d tec -c questoes --mode merge --jsonArray --file [filePath]

## Acessar DB tec
```
use tec
```

## Consultar por assunto
```js
let materia = "Auditoria"
let banca = "CESPE"
let anoBase = 2015

let total = db.questoes.find({
  "questao.nomeMateria": materia,
  "questao.bancaSigla": banca,
  "questao.concursoAno": { $gte: anoBase }
}).count()

db.questoes.aggregate([
  { $match: { "questao.nomeMateria": materia, "questao.bancaSigla": banca, "questao.concursoAno": { $gte: anoBase } } },
  { $group: { _id: "$questao.nomeAssunto", count: { $sum: 1 } } },
  { $addFields: { perc: { $trunc: { $multiply: [{ $divide: ["$count", total] }, 100] } } } }, 
  { $sort: { count: -1 } }
])
```