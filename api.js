// const fetchPrintQuestions = async (headers, bookId, start, length) => {
//   const body = new FormData()
//   body.append('configuracoes.idCadernoQuestoes', bookId)
//   body.append('configuracoes.idTeoriaModulo', '')
//   body.append('configuracoes.idTeoriaAssunto', '')
//   body.append('configuracoes.questaoInicial', start)
//   body.append('configuracoes.numeroQuestoes', length)
//   body.append('configuracoes.removerResolvidas', 'false')
//   body.append('configuracoes.removerAcertadas', 'false')
//
//   headers = Object.assign(
//     { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
//     headers
//   )
//
//   const method = 'POST'
//
//   const res = await fetch(`https://www.tecconcursos.com.br/questoes/cadernos/${PLANO}/${bookId}/ajaxCarregarQuestoesImpressao`, {
//     method,
//     headers,
//     body
//   })
//
//   // TODO: proper error handling
//   if (res.status !== 200) {
//     throw new Error(`Unexpected response code ${res.status}`)
//   }
//
//   return res.json()
// }

// const fetchQuestionDetails = async (headers, question) => {
//   let res = await fetch(`https://www.tecconcursos.com.br/api/questoes/${question.idQuestao}`, {
//     headers
//   })
//
//   // TODO: proper error handling
//   if (res.status !== 200) {
//     throw new Error(`Unexpected response code ${res.status}`)
//   }
//
//   const apiQuestion = await res.json()
//   apiQuestion.questao.cargoDescricao = apiQuestion.questao.cargoSigla
//
//   res = await fetch(`https://www.tecconcursos.com.br/api/questoes/${question.idQuestao}/detalhes`, {
//     headers
//   })
//
//   // TODO: proper error handling
//   if (res.status !== 200) {
//     throw new Error(`Unexpected response code ${res.status}`)
//   }
//
//   const apiInfo = await res.json()
//
//   res = await fetch(`https://www.tecconcursos.com.br/api/questoes/${question.idQuestao}/comentario`, {
//     headers
//   })
//
//   // TODO: proper error handling
//   if (res.status !== 200) {
//     throw new Error(`Unexpected response code ${res.status}`)
//   }
//
//   const apiComment = await res.json()
//
//   return Object.assign({ questao: question }, apiQuestion, apiInfo, apiComment)
// }