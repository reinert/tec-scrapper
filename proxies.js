const puppeteer = require('puppeteer')
const fs = require('fs')

const VIEWPORT = { width: 1080, height: 720 }

const func = `
function mySelect(content) {
  let s = Array.from(document.querySelectorAll('option')).find(el => el.textContent === content);
  s.selected = true;
  s.parentElement.onchange();
}
`

const getIps = `
function getIps() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(1)'));
  return tds.map(el => el.children.length > 1 ? el.children[1].innerText : null).filter(vl => vl != null);
}
`

const getTypes = `
function getTypes() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(2)'));
  tds.shift();
  tds.shift();
  return tds.map(el => el.innerText);
}
`

const getAnons = `
function getAnons() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(3)'));
  tds.shift();
  tds.shift();
  return tds.map(el => el.innerText);
}
`

const getLocations = `
function getLocations() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(4)'));
  tds.shift();
  return tds.map(el => el.innerText);
}
`

const getHostnames = `
function getHostnames() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(5)'));
  tds.shift();
  return tds.map(el => el.innerText);
}
`

const getSpeeds = `
function getSpeeds() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(7)'));
  tds.shift();
  return tds.map(el => parseInt(el.firstElementChild.firstElementChild.width));
}
`

const getUptimes = `
function getUptimes() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(8)'));
  tds.shift();
  return tds.map(el => {
    let uptime = el.firstElementChild.firstElementChild.title.match(/\\d+\\sof\\s\\d+/)[0].split(' of ');
    return { up: parseInt(uptime[0]), total: parseInt(uptime[1]) };
  });
}
`

const getDates = `
function getDates() {
  let opt = Array.from(document.querySelectorAll('option')).find(el => el.textContent === 'ANM&HIA');
  let tbody = opt.parentElement.parentElement.parentElement.parentElement.parentElement;
  let tds = Array.from(tbody.querySelectorAll('td:nth-child(9)'));
  tds.shift();
  return tds.map(el => {
    let split = el.children[0].innerText.split(' (');
    return { date: split[0], pastTime: split[1].slice(0, -1) };
  });
}
`

async function getPage (browser) {
    const pages = await browser.pages()
    let page = pages.pop()
    if (!page) {
        page = await browser.newPage()
        page.setViewport(VIEWPORT)
    }
    return page
}

let b = null

async function main() {
  const browser = await puppeteer.launch({
    headless: true,
    // args: ['--proxy-server=socks5://127.0.0.1:3123']
  })

  b = browser

  const page = await getPage(browser)

  // await page.goto('https://api.ipify.org')
  await page.goto('http://spys.one/free-proxy-list/BR/')

  await page.addScriptTag({ content: func })
  await page.evaluate(`mySelect('500')`)
  await page.waitForNavigation()

  await page.addScriptTag({ content: func })
  await page.evaluate(`mySelect('ANM&HIA')`)
  await page.waitForNavigation()

  // await page.addScriptTag({ content: func })
  // await page.evaluate(`mySelect('HTTP')`)
  // await page.waitForNavigation()

  await page.addScriptTag({ content: getIps })
  const ips = await page.evaluate(`getIps()`)
  await page.addScriptTag({ content: getTypes })
  const types = await page.evaluate(`getTypes()`)
  await page.addScriptTag({ content: getAnons })
  const anons = await page.evaluate(`getAnons()`)
  await page.addScriptTag({ content: getLocations })
  const locations = await page.evaluate(`getLocations()`)
  await page.addScriptTag({ content: getHostnames })
  const hostnames = await page.evaluate(`getHostnames()`)
  await page.addScriptTag({ content: getSpeeds })
  const speeds = await page.evaluate(`getSpeeds()`)
  await page.addScriptTag({ content: getUptimes })
  const uptimes = await page.evaluate(`getUptimes()`)
  await page.addScriptTag({ content: getDates })
  const checkDates = await page.evaluate(`getDates()`)

  const list = []
  for (const i in ips) {
    list.push({
      ip: ips[i],
      type: types[i],
      anon: anons[i],
      location: locations[i],
      hostname: hostnames[i],
      speed: speeds[i],
      uptime: uptimes[i],
      checkDate: checkDates[i]
    })
  }

  fs.writeFile(`ips.json`, JSON.stringify(list), (err) => {
    if (err) {
      console.log(`Failed to write ips.json`)
      console.log(`Error message: ${err.message}`)
    } else {
      console.log(`ips.json written successfully.`)
    }

    browser.close()
  })
}

main().catch(e => {
  console.log('GENERAL ERROR')
  console.log(e)
  b.close()
})
