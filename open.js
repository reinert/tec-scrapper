const puppeteer = require('puppeteer')

async function main(proxy) {
  let args = null
  if (proxy) {
    args = [ `--proxy-server=${proxy}` ]
  }

  const browser = await puppeteer.launch({
    headless: false,
    dumpio: true,
    slowMo: 180,
    args: args
  })

  if (proxy) {
    const pages = await browser.pages()
    await pages[0].goto('https://whatismyip.com.br/')
  }
}

const proxy = process.argv[2]

main(proxy)
  .catch(console.log)
