import test from 'ava'
import low from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'
import lodashId from 'lodash-id'
import cheerio from 'cheerio'
import fs from 'fs'

function setupDb() {
  const adapter = new FileSync('db/db-test.json')
  const db = low(adapter)
  db._.mixin(lodashId)
  db._.id = 'idQuestao'
  db.setState({
    mixes: [],
    questoes: [],
    questoesImpressao: [],
    questoesHtml: [],
    headHtml: null
  }).write()
  return db
}

const db = setupDb()

test('update by id not overwriting', async t => {
  const a = {
    id: 1,
    idQuestao: 1,
    questao: {
      orgaoSigla: 'TCU',
      cargoSigla: 'AUD (TCU)'
    }
  }

  const b = {
    id: 1,
    idQuestao: 1,
    questao: {
      bancaSigla: 'CESPE',
      orgaoSigla: 'TCU',
      cargoDescricao: 'Auditor Federal de Controle Externo',
    }
  }

  const expected = {
    id: 1,
    idQuestao: 1,
    questao: {
      bancaSigla: 'CESPE',
      orgaoSigla: 'TCU',
      cargoDescricao: 'Auditor Federal de Controle Externo',
      cargoSigla: 'AUD (TCU)'
    }
  }

  // persist
  db.get('mixes')
    .push(a)
    .write()

  let q = db.get('mixes').getById(1).value()
  Object.assign(q.questao, b.questao) // Automatically updates DB
  db.write()

  // check
  t.deepEqual(db.get('mixes').value()[0], expected)
})

test('persist print questions', async t => {
  // read print-questions
  const questions = require('./templates/json/print-questions.json')

  // persist
  db.get('questoesImpressao')
    .push(...questions.list)
    .write()

  // check
  t.deepEqual(db.get('questoesImpressao').value(), questions.list)
})

test('persist question', async t => {
  // read question, info and comment
  const question = require('./templates/json/question.json')
  const info = require('./templates/json/info.json')
  const comment = require('./templates/json/comment.json')

  const q = {
    'idQuestao': question.questao['idQuestao'],
    'questao': question.questao,
    'informacoes': info.informacoes,
    'comentario': comment.comentario
  }

  // persist
  db.get('questoes')
    .push(q)
    .write()

  // check
  t.deepEqual(db.get('questoes').getById(q.idQuestao).value(), q)
})

test('persist html question', async t => {
  let $ = cheerio.load(fs.readFileSync('./templates/html/questao-com-info.html'))

  const idQuestao = $('.id-questao').text().substr(1)
  const questao = $.html($('.questao')) // $('.questao').outerHTML
  const informacoes = $.html($('.questao-complementos')) // $('.questao-complementos').outerHTML

  $ = cheerio.load(fs.readFileSync('./templates/html/questao-com-comentario.html'))
  const comentario = $.html($('.questao-complementos')) // $('.questao-complementos').outerHTML
  const head = $.html($('head')) // $('head').outerHTML

  db.get('questoesHtml')
    .push({ idQuestao, questao, informacoes, comentario })
    .write()

  db.set('headHtml', head)
    .write()

  t.is(db.get('questoesHtml').getById(idQuestao).value().questao, questao)
})
