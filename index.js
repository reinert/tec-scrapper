const puppeteer = require('puppeteer')
const applyEvasions = require('headless-cat-n-mouse/apply-evasions')
const fs = require('fs')
const low = require('lowDB')
const FileAsync = require('lowDB/adapters/FileAsync')
const lodashId = require('lodash-id')
const debug = require('debug')

let BROWSER = null
let PAGE = null
let DB = null
let USER = null
let BOOK_ID = null

let START_TIME = 0
let STOP_TIME = 0

let PRINT_ONLY = true

let PLANO = 'experimental'
let VIEWPORT = { width: 980, height: 1080 }
let RANGE = 200
let INTERCEPT_ALLOWED = false
let SLOW_FROM = 8000
let SLOW_TO = 20000
// 2-6s = 5q/min

const rand = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

const slowMo = () => rand(SLOW_FROM, SLOW_TO)

const think = async (d, page, fraction = 1) => {
  const waitTime = slowMo() / fraction
  d(`Thinking for ${(waitTime / 1000).toFixed(1)}s...`)
  return page.waitFor(waitTime)
}

const setInterceptAllowed = (d, value) => {
  if (!d) d = debug('tec:main')
  INTERCEPT_ALLOWED = value
  d(`INTERCEPT_ALLOWED = ${INTERCEPT_ALLOWED}`)
}

const isInterceptAllowed = () => INTERCEPT_ALLOWED

let ATTEMPT_SUCCESS = true
let ATTEMPT = 0
const TOTAL_ATTEMPTS = 3

function tryLogin() {
  ATTEMPT++

  if (ATTEMPT <= TOTAL_ATTEMPTS && !ATTEMPT_SUCCESS) {
    console.log(`Attempt ${ATTEMPT} of ${TOTAL_ATTEMPTS} to login...`)
    const wait = Math.pow(ATTEMPT, 2) * 15000
    console.log(`Trying to access login page in ${wait/1000}s...`)
    setTimeout(() => {
      PAGE.goto('https://www.tecconcursos.com.br/login')
        .then(() => {
          ATTEMPT_SUCCESS = true
          ATTEMPT = 0
          console.log('Successful recovery!')
          console.log('Resuming scrape now.')
        })
        .catch(e => {
          console.log('Could not access login page.')
          console.log(e)
          tryLogin()
        })
    }, wait)
  } else {
    console.log(`Couldn't recover from error. Closing browser now.`)
    // BROWSER.close()
    process.exit(1)
  }
}

function onRejected(e, source) {
  console.log('UNCAUGHT ERROR')
  console.log(e)

  console.log(`SOURCE: ${source || 'Main'}`)

  ATTEMPT_SUCCESS = false

  tryLogin()
}

async function waitForNetworkIdle(d, page, timeout = 5000) {
  try {
    d(`Waiting ${timeout}ms for network idle...`)
    await page.waitForNavigation({ networkIdleTimeout: timeout, waitUntil: 'networkidle' })
  } catch (e) {
    d('Network Idle TIMEOUT')
  }
  return page
}

async function getPage(browser) {
  let d = debug('tec:browser')

  d('Obtaining last browser page...')
  const pages = await browser.pages()
  let page = pages.pop()

  if (!page) {
    d('No pages found. Opening a new one...')
    page = await browser.newPage()
  }

  await applyEvasions(page)

  d(`Setting page viewport to ${VIEWPORT.width}x${VIEWPORT.height}`)
  await page.setViewport(VIEWPORT)

  return page
}

async function blockAnalytics(d, page) {
  /*
  Block the following URLs:
  - https://www.googleadservices.com
  - https://www.google-analytics.com
  - https://www.google.com[.br]/ads
   */

  await page.setRequestInterceptionEnabled(true)

  page.on('request', req => {
    if (Math.max(
        // req.url.indexOf('googleadservices'),
        // 'googleads.g'
        // 'google.com[.br]/ads'
        req.url.indexOf('google-analytics'),
        req.url.indexOf('ads')
      ) > -1) {
      req.abort()
      d(`Request aborted: ${req.url}`)
    } else {
      req.continue()
    }
  })
}

async function openBook() {
  let d = debug('tec:openBook')

  d('Accessing book page...')

  try {
    await PAGE.goto(`https://www.tecconcursos.com.br/questoes/cadernos/${PLANO}/${BOOK_ID}`,
      { timeout: 5000 })
    // d('Book Page accessed successfully.')
  } catch (e) {
    d('Book Page access TIMEOUT.')
  }

  // try {
  //   await PAGE.keyboard.press('.')
  //   d('Timer paused.')
  // } catch (e) {
  //   d('Could not pause timer.', e)
  // }
}

async function openPrint(initialQuestion = 1, range = RANGE) {
  let d = debug('tec:openPrint')

  d('Accessing print page...')

  // await PAGE.goto(`https://www.tecconcursos.com.br/questoes/cadernos/${PLANO}/${BOOK_ID}/configurar-impressao`)
  // await PAGE.click('button.toolbar-botao.glyphicon-option-horizontal')
  // await PAGE.evaluate(`document.querySelector('button[data-original-title="Mais..."]').click()`)
  await PAGE.evaluate(`document.querySelector('button.toolbar-botao.glyphicon-option-horizontal').click()`)
  // await waitForNetworkIdle(d, PAGE, 3000)
  await PAGE.waitFor(2000)
  // await PAGE.click('button.toolbar-botao.glyphicon-print')
  // await PAGE.evaluate(`document.querySelector('button[data-original-title="Imprimir"]').click()`)
  await PAGE.evaluate(`document.querySelector('button.toolbar-botao.glyphicon-print').click()`)
  await waitForNetworkIdle(d, PAGE, 3000)

  d('Page accessed. Now tweaking...')

  await PAGE.waitFor(1000)
  try {
    await PAGE.evaluate(`document.querySelector('input[type=range]#questaoInicial').value = ${initialQuestion}`)
  } catch (e) {
    d('FAILED TO TWEAK. Trying once again...')
    await PAGE.waitFor(1500)
    await PAGE.evaluate(`document.querySelector('input[type=range]#questaoInicial').value = ${initialQuestion}`)
  }
  d(`Set #questaoInicial range to ${initialQuestion}`)
  await PAGE.waitFor(50)
  await PAGE.evaluate(`document.querySelector('input#questaoInicialInput').value = ${initialQuestion}`)
  d(`Set #questaoInicialInput to ${initialQuestion}`)
  await PAGE.waitFor(50)
  await PAGE.evaluate(`document.querySelector('input[type=range]#numeroQuestoes').value = ${range}`)
  d(`Set #numeroQuestoes range to ${range}`)
  await PAGE.waitFor(50)
  await PAGE.evaluate(`document.querySelector('input#numeroQuestoesInput').value = ${range}`)
  d(`Set #numeroQuestoesInput to ${range}`)
  await PAGE.waitFor(250)
  // await PAGE.click('input#gabaritoPorQuestao')
  // await PAGE.waitFor(100)

  d('Clicking confirm...')

  await PAGE.click('button#confirmar-button')
}

async function interceptPrintPage(target) {
  let d = debug('tec:print-page')

  const type = await target.type()
  const url = await target.url()

  // d(`Target created: ${type} at ${url}`)

  if (type === 'page') {
    const printPage = await target.page()
    // await blockAnalytics(d, printPage)

    if (url.indexOf('imprimir') > -1) {
      d(`Setting response listener to ${url}...`)

      printPage.on('response', res =>
        interceptFigure(res).catch(e => onRejected(e, 'interceptFigurePrintPage')))

      printPage.on('response', async res => {
        let d = debug('tec:print-page')

        if (res.url.indexOf('ajaxCarregarQuestoesImpressao') > -1) {
          d('Response received! Committing to DB...')

          const questions = await res.json()

          for (let q of questions.list) {
            DB.get('questoes')
              .upsert({ id: q.idQuestao, questao: q })
              .commit()
          }

          DB.write()
          d('WRITE')

          updateLastPrint(d, questions.list[0].numeroQuestaoAtual)

          setInterceptAllowed(d, !PRINT_ONLY)

          await think(d, printPage, 0.75)

          await printPage.close()

          if (PRINT_ONLY) {
            const lastSeen = questions.list[questions.list.length-1].numeroQuestaoAtual + 1
            await think(d, PAGE, 0.25)
            updateLastSeen(d, lastSeen) // must happen after interceptBook is triggered by the site's auto-redirect
            return startScraping(d)
          }
        }
      })
    }
  }
}

function updateLastPrint(d, idQuestaoCaderno) {
  d(`Setting LAST_PRINT to ${idQuestaoCaderno}...`)

  DB.set('caderno.lastPrint', idQuestaoCaderno).write()

  d('WRITE')
}

function updateLastSeen(d, idQuestaoCaderno) {
  d(`Setting LAST_SEEN to ${idQuestaoCaderno}...`)

  DB.set('caderno.lastSeen', idQuestaoCaderno).write()

  d('WRITE')
}

async function interceptBook(res) {
  let d = debug('tec:book')

  const matchCaderno = res.url.match(/\/api\/cadernos\/\d+\/carregar/)
  if (matchCaderno) {
    const split = matchCaderno[0].split('/')
    const idCaderno = parseInt(split[split.length - 2])

    d(`Reading book ${idCaderno}...`)

    const json = await res.json()
    let caderno = json.caderno
    caderno.id = idCaderno

    d('Writing...')
    // d(caderno)

    caderno = DB.get('caderno').assign(caderno).value()

    DB.write()
    d('WRITE')

    if (caderno.lastSeen >= caderno.numeroTotalQuestoes) {
      exit()
    }

    const printBreak = ((caderno.lastSeen - 1) % RANGE === 0)
      && (caderno.lastPrint < caderno.lastSeen)

    if (printBreak) {
      setInterceptAllowed(d, false)
      await PAGE.waitFor(3000)
      await openPrint(caderno.lastSeen)
    } else if (!isInterceptAllowed() && !PRINT_ONLY) {
      setInterceptAllowed(d, true)
    }
  }
}

async function interceptDiscussion(res) {
  if (!isInterceptAllowed()) return

  const matchDiscussion = res.url.match(/\/api\/discussoes\/\d+\/comentarios-alunos/)
  if (matchDiscussion) {
    let d = debug('tec:discussion')

    const split = matchDiscussion[0].split('/')
    const idQuestao = parseInt(split[split.length - 2])

    d(`Reading discussion of question ${idQuestao}...`)

    const questao = await res.json()

    questao.id = idQuestao

    d('Committing...')
    // d(questao)

    const updated = DB.get('questoes').updateById(idQuestao, questao).value()
    if (!updated) DB.get('questoes').insert(questao).commit()

    DB.write()
    d('WRITE')

    await think(d, PAGE, 2)

    d('Opening info...')
    await PAGE.keyboard.press('i')
  }
}

async function interceptQuestion(res) {
  if (!isInterceptAllowed()) return

  // Scrape info
  const matchInfo = res.url.match(/\/api\/questoes\/\d+\/detalhes/)
  if (matchInfo) {
    let d = debug('tec:info')

    const split = matchInfo[0].split('/')
    const idQuestao = parseInt(split[split.length - 2])

    d(`Reading info of question ${idQuestao}...`)

    const questao = await res.json()

    questao.id = idQuestao
    questao.updatedAt = new Date().toJSON()

    d('Committing...')
    // d(questao)

    const updated = DB.get('questoes').updateById(idQuestao, questao).value()
    if (!updated) DB.get('questoes').insert(questao).commit()

    DB.write()
    d('WRITE')

    await think(d, PAGE, 3)

    // Check if should trigger print or stop
    const caderno = DB.get('caderno').value()
    const total = caderno.numeroTotalQuestoes
    let atual = caderno.lastSeen

    if (atual === total) {
      exit()
    }

    if (atual % RANGE === 0) {
      atual = atual + 1
      updateLastSeen(d, atual)
      const currentRange = Math.min(total - atual, RANGE)
      d(`Opening print page from ${atual} with length of ${currentRange}...`)
      setInterceptAllowed(d, false)
      // Go to next question so it won't loop infinity after print
      // await PAGE.click('button.questao-navegacao-botao-proxima')
      await PAGE.keyboard.press('ArrowRight')
      await waitForNetworkIdle(d, PAGE)

      await openPrint(atual, currentRange)

      return
    }

    if ((atual - 1) % RANGE === 0) {
      d('Waiting a little more for next question because print has just been triggered.')
      await think(d, PAGE, 0.5)
    }

    // Go to next question
    d(`Next question...`)
    // await PAGE.click('button.questao-navegacao-botao-proxima')
    await PAGE.keyboard.press('ArrowRight')
    return
  }

  // Scrape comment
  const matchComentario = res.url.match(/\/api\/questoes\/\d+\/comentario/)
  if (matchComentario) {
    let d = debug('tec:comment')

    const split = matchComentario[0].split('/')
    const idQuestao = parseInt(split[split.length - 2])

    d(`Reading comment of question ${idQuestao}...`)

    const questao = await res.json()

    questao.id = idQuestao

    d('Committing...')
    // d(questao)

    const updated = DB.get('questoes').updateById(idQuestao, questao).value()
    if (!updated) DB.get('questoes').insert(questao).commit()

    DB.write()
    d('WRITE')

    await think(d, PAGE)

    const shouldRate = !rand(0, 4)
    if (shouldRate) {
      d(`Rating question ${idQuestao}...`)
      try {
        await PAGE.click('span[tec-tooltip="Excelente"]')
        await waitForNetworkIdle(d, PAGE, 3000)
        d(`Rate done.`)
      } catch (e) {
        d('Failed to rate.')
      }
    }

    const shouldOpenDiscussion = !!updated.questao.numeroPosts
    if (shouldOpenDiscussion) {
      d('Opening discussion...')
      // await PAGE.click('i.icone.ajuste.discussao')
      await PAGE.keyboard.press('f')
      return
    }

    d('Opening info...')
    // await PAGE.click('i.info')
    await PAGE.keyboard.press('i')
    return
  }

  // Scrape question
  const matchQuestao = res.url.match(/\/api\/cadernos\/\d+\/questoes\/\d+/)
  if (matchQuestao) {
    let d = debug('tec:question')

    const split = matchQuestao[0].split('/')
    // const idCaderno = parseInt(split[split.length - 3])
    const idQuestaoCaderno = parseInt(split[split.length - 1])

    let questao = await res.json()

    d(`Reading question ${idQuestaoCaderno} with ID=${questao.questao.idQuestao}...`)

    d('Committing...')
    // d(questao)

    questao.questao.cargoDescricao = questao.questao.cargoSigla
    delete questao.questao.cargoSigla

    let stored = DB.get('questoes').getById(questao.questao.idQuestao).value()
    if (stored) {
      Object.assign(stored.questao, questao.questao)
      stored.idQuestaoCaderno = idQuestaoCaderno
      questao = stored
    } else {
      questao.id = questao.questao.idQuestao
      questao.idQuestaoCaderno = idQuestaoCaderno
      DB.get('questoes').insert(questao).commit()
    }

    d('Commit done.')

    // Write is done by #updateLastSeen
    // DB.write()
    // d('WRITE')

    updateLastSeen(d, idQuestaoCaderno)

    await think(d, PAGE, 2)

    const shouldAnswer = !rand(0, 2)
    if (shouldAnswer && !questao.questao.alternativaSelecionada) {
      d(`Answering question...`)
      const totalAlt = questao.questao.alternativas.length
      const alt = rand(0, totalAlt - 1)
      d(`Selected alternative ${alt + 1} of ${totalAlt}.`)
      try {
        // await PAGE.click(`input[type=radio]#alternativa-${alt}`)
        await PAGE.keyboard.press(getLetterOfAlternative(alt, totalAlt))
        await PAGE.waitFor(rand(300, 600))
        // await PAGE.click('button.botao-resolver')
        await PAGE.keyboard.press('Enter')
        await waitForNetworkIdle(d, PAGE, 3000)
        d(`Answer done.`)
      } catch (e) {
        d('Failed to answer.')
      }
    }

    const shouldOpenComment = !!questao.questao.possuiComentario
    if (shouldOpenComment) {
      d('Opening comment...')
      // await PAGE.click('i.comentario')
      await PAGE.keyboard.press('o')
      return
    }

    const shouldOpenDiscussion = !!questao.questao.numeroPosts
    if (shouldOpenDiscussion) {
      d('Opening discussion...')
      // await PAGE.click('i.icone.ajuste.discussao')
      await PAGE.keyboard.press('f')
      return
    }

    d('Opening info...')
    // await PAGE.click('i.info')
    await PAGE.keyboard.press('i')
    return
  }
}

const MULTIPLE_CHOICE = ['a', 'b', 'c', 'd', 'e']
const RIGHT_OR_WRONG = ['c', 'e']
function getLetterOfAlternative(chosen, total) {
  if (total === 5) {
    return MULTIPLE_CHOICE[chosen]
  }
  return RIGHT_OR_WRONG[chosen]
}

async function interceptFigure(res) {
  // Scrape question
  const matchFigura = res.url.match(/figuras\.tecconcursos\.com\.br\/.+/)
  if (matchFigura) {
    let d = debug('tec:figure')

    const imgId = matchFigura[0].split('/')[1]

    d(`Reading figure ${imgId}...`)

    const imgBuffer = await res.buffer()

    d(`Writing to disk...`)

    fs.writeFile(`img/${imgId}`, imgBuffer, { flag: 'wx' }, (err) => {
      if (err) {
        d(`Failed to write figure ${imgId}.`)
        d(err.message)
      } else {
        d(`Figure ${imgId} written successfully.`)
      }
    })
  }
}

function exit() {
  let d = debug('tec:main')

  STOP_TIME = Date.now()

  let totalTimeInMinutes = (STOP_TIME - START_TIME) / 60000

  d('Scrapping finished successfully.')
  d(`Total time: ${totalTimeInMinutes.toFixed(1)}m.`)

  // PAGE.close()
  // BROWSER.close()
  process.exit()
}

async function startScraping(d) {
  return openBook()
  // const book = DB.get('caderno').value()
  //
  // if (book.lastSeen >= book.numeroTotalQuestoes) {
  //   exit()
  // }
  //
  // const printBreak = ((book.lastSeen - 1) % RANGE === 0)
  //                    && (book.lastPrint < book.lastSeen)
  //
  // if (printBreak) {
  //   await openBook()
  //   d('-------------------- FIRST --------------------')
  //   await PAGE.waitFor(5000)
  //   await openPrint(book.lastSeen)
  //   d('-------------------- SECOND --------------------')
  // } else {
  //   setInterceptAllowed(d, true)
  //   await openBook()
  //   d('-------------------- OUT --------------------')
  // }
}

async function interceptLogin(res) {
  if (res.url.indexOf(`tecconcursos`) > -1
      && res.url.endsWith('/login')
      && res.request().method === 'GET') {
    let d = debug('tec:login')

    setInterceptAllowed(d, false)

    d('LOGIN REQUESTED')

    d('Login...')
    await waitForNetworkIdle(d, PAGE, 3000)
    // await PAGE.type('input#email', USER.email)
    await PAGE.evaluate(`document.querySelector('input#email').value = '${USER.email}'`)
    await PAGE.waitFor(250)
    // await PAGE.type('input#senha', USER.password)
    await PAGE.evaluate(`document.querySelector('input#senha').value = '${USER.password}'`)
    await PAGE.waitFor(250)
    await PAGE.click('button#entrar-site')
    await PAGE.waitForNavigation()

    d('Login success!')
  }
}

async function main(wsEndpoint, user, bookId,
                    {
                      dbName = bookId,
                      range = RANGE,
                      slowMo = { from: SLOW_FROM, to: SLOW_TO },
                      viewport = VIEWPORT,
                      plano = PLANO
                    } = {}) {
  START_TIME = Date.now()

  let d = debug('tec:main')

  if (!wsEndpoint) {
    d('wsEndpoint must be specified.')
    process.exit(1)
  }
  if (!user || !user.email || !user.password) {
    d('user must be specified.')
    process.exit(1)
  }
  if (!bookId) {
    d('bookId must be specified.')
    process.exit(1)
  }

  d('Initializing tec-scrapper...')

  USER = user
  d(`USER = ${user.email}`)

  RANGE = range
  d(`RANGE = ${RANGE}`)

  SLOW_FROM = slowMo.from
  SLOW_TO = slowMo.to
  d(`SLOW MO = (${SLOW_FROM}, ${SLOW_TO})`)

  VIEWPORT = viewport
  d(`VIEWPORT = (${VIEWPORT.width}, ${VIEWPORT.height})`)

  PLANO = plano
  d(`PLANO = ${PLANO}`)

  BOOK_ID = bookId
  d(`BOOK = ${BOOK_ID}`)

  d(`DB = ${dbName}`)

  d(`PRINT_ONLY = ${PRINT_ONLY}`)

  d('Connecting to DB...')
  DB = await low(new FileAsync(`db/${dbName}.json`))
  DB._.mixin(lodashId)
  DB.defaults({ caderno: { lastPrint: 0, lastSeen: 1 }, questoes: [] }).write()
  d('DB connected.')

  d(`Connecting to Browser on the url ${wsEndpoint}...`)
  BROWSER = await puppeteer.connect({ browserWSEndpoint: wsEndpoint })
  d('Browser connected.')

  PAGE = await getPage(BROWSER)
  // await blockAnalytics(d, PAGE)

  BROWSER.on('targetcreated', t =>
    interceptPrintPage(t).catch(e => onRejected(e, 'interceptPrintPage')))

  PAGE.on('response', res =>
    interceptLogin(res).catch(e => onRejected(e, 'interceptLogin')))

  PAGE.on('response', res =>
    interceptBook(res).catch(e => onRejected(e, 'interceptBook')))

  PAGE.on('response', res =>
    interceptDiscussion(res).catch(e => onRejected(e, 'interceptDiscussion')))

  PAGE.on('response', res =>
    interceptQuestion(res).catch(e => onRejected(e, 'interceptQuestion')))

  PAGE.on('response', res =>
    interceptFigure(res).catch(e => onRejected(e, 'interceptFigure')))

  // return PAGE.goto('https://www.tecconcursos.com.br/login')
  return startScraping(d)
}

const wsEndpoint = process.argv[2].replace(':\\', '://').replace(/\\/g, '/')
const email = process.argv[3]
const password = process.argv[4]
const bookId = process.argv[5]
const dbName = process.argv[6]
if (process.argv.length > 7) {
  // 8th argument is treated like 'scrape comments'
  PRINT_ONLY = ['false', 'False', '0'].indexOf(process.argv[7]) > -1
}
if (process.argv.length > 8) {
  // 9th and 10th arguments are slow from to
  SLOW_FROM = parseInt(process.argv[8]) * 1000
  SLOW_TO = parseInt(process.argv[9]) * 1000
}
main(wsEndpoint, { email, password }, bookId, { dbName })
  .catch(onRejected)
