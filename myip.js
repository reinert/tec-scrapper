const puppeteer = require('puppeteer')
const ips = require('./ips.json')

const VIEWPORT = { width: 1080, height: 720 }

async function getPage (browser) {
    const pages = await browser.pages()
    let page = pages.pop()
    //let page = null
    if (!page) {
        page = await browser.newPage()
        page.setViewport(VIEWPORT)
    }
    return page
}

async function main(proxyIdx) {
  let args = null
  if (proxyIdx != null) {
    args = [`--proxy-server=${ips[proxyIdx]}`]
  }
  const browser = await puppeteer.launch({
    headless: false
    ,args: args
  })

  const page = await getPage(browser)

  // await page.goto('https://api.ipify.org')
  await page.goto('https://whatismyip.com.br/')
}

main(process.argv[2])
